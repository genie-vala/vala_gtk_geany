# valagtkgeany
This program creates a vala/Gtk+ helloworld project which can be compiled using [geany](https://github.com/geany/geany) and [meson](https://mesonbuild.com/) build system. It is also written in vala. Output project tree is the same as of gnome-builder.

## Features
* it creates a helloworld project containing a window.ui (by [Glade](https://github.com/GNOME/glade)) and its controller class.
* output project can be opened and compiled with [geany](https://github.com/geany/geany) (compile, build, and clean shortcuts of geany are associated with meson and ninja commands).
* it creates proper paths according to posix and windows systems.
* Min Gtk+ version can be choosen.
* ./data folder is not compiled when on windows.
* Commandline version is also available use ./createvala4geany --help after building the project
* Geany plugin (tested on both windows 10 and ubuntu 16.04) 

## Requirements

* meson >= 0.40.0
* gio-2.0 >= 2.48.2
* gtk+-3.0 >= 3.20

## Tested on
* Windows 10 64 bit, MSYS2 MinGW64 (gtk+ and other libs were installed via pacman. \msys64\mingw64\bin must be in your path when geany is started.)
* Ubuntu 16.04 LTS (gtk+ and other libs were installed via apt-get, flatpak repo was not used)


## How to build
clone the project and:
```
cd valagtkgeany
mkdir build
cd build
meson ..
ninja
```
Precompiled binary for Windows 64bit users can be found in tag section

## Commandline interface

Options:
```
  -v, --version             Display version number
  -o, --directory=~/        root directory of the project
  -n, --name=helloworld     set name of the project
  -i, --giover=2.48.2       set minimum gio version
  -k, --gtkver=3.20         set minimum gtk+ version
```
Example usage of commandline:

```
./createvala4geany --directory="C:\Users\user\projects" --name=helloworld --gtkver=3.20 --giover=2.48.2
```
or shorter:
```
./createvala4geany -o "C:\Users\user\projects" -n helloworld -k 3.20 -i 2.48.2
```
All arguments have their defaults, so you can simply input ```./createvala4geany``` to create a "helloworld" project in your home directory.

## Preview:
![alt text](./preview/output.png?raw=true)
